---
date: 220624
author: Thomas Hartmann
---

# I added a blog

Because I keep on adding tutorial, I added the blog functionality here, thanks to [ABlog](https://ablog.readthedocs.io).
So, every time you come back to this website, you can instantly see, what is new!