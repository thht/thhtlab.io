---
date: 221121
author: Thomas Hartmann
---

# Find me on Mastodon

I decided to follow the pack towards Mastodon. You can find me [via this link](https://mastodon.world/@thht)
or using the handle `@thht@mastodon.world`.