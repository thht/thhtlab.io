# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Website of Dr. Thomas Hartmann'
html_title = 'Dr. Thomas Hartmann'
copyright = '2022, Thomas Hartmann'
author = 'Thomas Hartmann'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'myst_nb',
    'sphinx_comments',
    'ablog'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', '*import_posts*', '**/pandoc_ipynb/inputs/*', '.nox/*', 'README.md']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'pydata_sphinx_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_theme_options = {
    'github_url': 'https://github.com/thht/',
    'gitlab_url': 'https://gitlab.com/thht/',
    'twitter_url': 'https://twitter.com/thht80',
    'icon_links': [
        {
            'name': 'Mastodon',
            'url': 'https://mastodon.world/@thht',
            'icon': 'fa-brands fa-mastodon',
            'attributes':
                {
                    'rel': 'me'
                }
        },
        {
            'name': 'orcid',
            'url': 'https://orcid.org/0000-0002-8298-8125',
            'icon': 'fab fa-orcid',
            'type': 'fontawesome'
        }
    ],
    'search_bar_text': 'Search this site...',
}

html_sidebars = {
    '**': [
        'sidebar-nav-bs.html',
        'search-field.html',
        'postcard.html',
        'recentposts.html'
    ]
}

myst_enable_extensions = [
    'smartquotes',
    'replacements',
    'substitution',
    'linkify'
]

comments_config = {
   "hypothesis": True
}

blog_post_pattern = 'blog/*.md'
post_date_format = '%y%m%d'

blog_base_url = 'https://thht.gitlab.io'

def setup(app):
    app.add_css_file('css/custom.css')