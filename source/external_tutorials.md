# External Tutorials

The tutorials I publish here are not meant to cover the basics of [python],
[MNE-Python], [scikit-learn], [numpy], [pandas], [git], or whatever software
or package we might use here. Other people did a much better job at it than
I could ever do.

Instead, the tutorials are tailored to rather specific use cases I see in
my work.

If you would like to start using this amazing software and learn about it, 
here are some good tutorials

## Python
* Free, interactive datacamp course. Starts from the basics and is tailored to
  data scientists (yes, you are a data scientist!): https://www.datacamp.com/courses/intro-to-python-for-data-science
* The official tutorial by the developers of python: https://docs.python.org/3/tutorial/

## MNE-Python
* Tutorials by the developers themselves: https://mne.tools/stable/auto_tutorials/index.html
* Workshop by Richard Höchenberger:  https://www.youtube.com/watch?v=t-twhNqgfSY

## git
* Datacamp course: https://app.datacamp.com/learn/courses/introduction-to-git
* Simple one: https://rogerdudler.github.io/git-guide/


[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/
[gitlab_sbg]: https://git.sbg.ac.at/
[github]: https://github.com
[python]: https://www.python.org/
[MNE-Python]: https://mne.tools
[scikit-learn]: https://scikit-learn.org
[numpy]: https://numpy.org/
[pandas]: https://pandas.pydata.org/