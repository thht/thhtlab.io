# How to work remotely with Visual Studio Code

## Why would you want to do this?

If you are a member of our group, you are familiar with developing your analyses on the **bomber**.
If you work for a different lab/group, you still might have access to some remote virtual machine
that provides you direct access to computing resources and/or data so you might want to develop
right there.

Until recently, you would need some kind of desktop connection (like X2go, remote desktop, or xpra)
in order to do this. [Visual Studio Code][VS Code] allows you to develop code directly on the remote
machine using a **local interface**.

This makes the whole experience much smoother!

## Install the `Remote Development` extension

Open [VS Code] and click on this button: ![vs_code_ext_button](/_static/img/vs_code_extensions.png)

Now, search for the `Remote Development` extension and install it if it is not yet installed.

## Connect your Visual Studio Code to the bomber

As you might already know, most things in [VS Code] are done via the 
`Command Palette`. Press `Ctrl+Shift+P`. This brings up a field where you can type
in. As soon as you start typing, it searches for the possible commands.

We need the command `Remote-SSH Connect to Host...`. So, choose it.

It is going to ask you for the hostname. Enter it like:

```
bXXXXXX@obob-bomber-XXXXX.hpc.sbg.ac.at
```

using your username and the hostname.

It is going to open a new window and work for a while. Take a look at the bottom left
corner of the new window. You will find a green patch there with the hostname you just entered.

Congratulations! Everything you do in this new window is done on the bomber!

```{admonition} Install the extensions on the bomber

If this is the first time connecting [VS Code] to your bomber, you need to
install the extensions there as well. Remember, [VS Code] is running on
the bomber now, you are just remote controlling it.

Click on this button: ![vs_code_ext_button](/_static/img/vs_code_extensions.png)

And install the following extensions:

* Juypter
* Python

```

## Forward plots from the bomber

This setup works nicely
out of the box as long as no figures are drawn that you want to interact
with. (Non-interactive figures are fine).

In order to solve this, we need to forward those windows to our local PC.

This is easy on Linux, quite easy on a Mac, and quite hard on Windows.
I will only cover Linux and Mac here.

### Set configuration options.

```{warning}
The following commands need to be executed on your local PC or laptop.
**Not** on the bomber!
```

Open a terminal and issue:

```
touch ~/.ssh/config
ls ~/.ssh/config
```

The second command will give you a complete path to a file. Open it with 
the editor of your choice.

Insert this:

```
Host obob-bomber-*.hpc.sbg.ac.at
        ForwardAgent yes
        ForwardX11 yes
        ForwardX11Trusted yes
```

### If you are on a Mac
Make sure you have [homebrew] installed.

Then in a terminal:

```
brew install --cask xquartz
```

And then reboot your system.

[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/
[gitlab_sbg]: https://git.sbg.ac.at/
[github]: https://github.com
[homebrew]: https://brew.sh/