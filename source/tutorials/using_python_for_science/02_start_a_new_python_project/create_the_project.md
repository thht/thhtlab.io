# Setup the project

This chapter will explain how to create an empty project on [gitlab] and 
start working on it.

## Create your project

Login to your [gitlab] account you created in the previous step.

Once you are logged in, you will see a small `+` icon at the top. If you
click it, you will see the option to create a new project/repository:

![create_new_repo](/_static/img/gitlab_new_project.png)

Click it and then choose "Create blank project".

Give it a name, choose the namespace (either matching your [gitlab] username
or the group you created previously), and leave all the other options alone.

Finally, click `Create` project`.

Congratulations! You just created your project.

## Put the project on your bomber and/or local PC
First, start [VS Code].

### Connect Visual Studio Code to your bomber (optional)

If you are a member of our group you most probably want to work on the bomber.
In this case, you first need to connect visual studio code to your bomber.

[This tutorial](../03_visual_studio_code_remote/introduction.md) tells you how to do it.

### Clone the project

Now we need to clone the project we just created.

```{admonition} But my project is already cloned!
No problem! In this case click on `File->Open Folder' and choose the folder where
your project is stored.
```
In order to do this, click on the Source Control icon: ![source_control_icon](/_static/img/vs_code_source_control.png)
and then `Clone Repository`.

Now go back to your web browser with [gitlab] open and make sure you are at the
page of the project we created above. Now click on `Clone`. This will open up
something like this:

![gitlab_clone](/_static/img/gitlab_clone.png)

Click on the icon that looks like a clipboard right next to the line that starts
with `git@gitlab.com:`. This copies the URL of the repository that [VS Code]
is asking for. Paste it in there and hit `Enter`.

You now need to choose where the project should live on your hard drive or the
storage of your bomber.

If you are on the bomber, you **must** choose a folder under `/mnt/obob/staff/jdoe`!

Please be aware that you are choosing the parent folder. This means that if
your project is called "my_analysis", a folder called "my_analysis" will be
created in the folder you are about to choose.

Now, click `Ok`, and wait a bit until the process is done. A small window is going to
ask you whether you want to open the cloned repository. Click `Open`!

You are now in the project folder and can start setting it up!

[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/
[gitlab_sbg]: https://git.sbg.ac.at/
[github]: https://github.com