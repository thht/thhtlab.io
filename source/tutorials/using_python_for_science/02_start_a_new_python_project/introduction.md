# How to start a new project

So, you sucessfully [prepared your computer and your bomber](../01_prepare_computer_for_scientific_python/introduction.md)
and now want to start a new analysis project? Great!

This tutorial will show you how to set up your project on [gitlab], get it on your
bomber and/or local PC, install a [conda] environment for it,
and connect VS Code] so you can work on it.

```{toctree}
:maxdepth: 1
create_the_project
add_boilerplate
create_the_environment
run_some_code
```

[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/
[gitlab_sbg]: https://git.sbg.ac.at/
[github]: https://github.com
