# Create the Python Environment for the Project

## What is a Python Environment again?

A [python] environment is a folder that contains the Python interpreter
as well as python packages. These are all independent of the python
interpreter and packages of the system. Furthermore, different
environments do not interfere with each other.

To use an environment, you need to **activate** it first. Once
activated, you and your scripts use the interpreter and the packages
of the environment.

## Rules for environments

From my personal experience working with [python] and [conda] environments
for years and providing support for other people who use them, it is 
helpful to follow some rules when it comes to environments.

Please note that common tutorials have slightly different approaches sometimes.

But trust me, doing it like this makes it harder to break an analysis of yours
and in case it breaks, it is easier to recover it.

### Rule 1: One Project, one Environment

Sharing environments between projects is maybe the number one source of
problems. Imagine two analyses sharing the same environment. Both run fine.

But now you need to add a package to it because Analysis A requires it. This
package pulls in other packages and updates a few. Chances are, Analysis B stops
working. Now you fix your environment for Analysis B. Analysis A is not working
anymore...

### Rule 2: Environments live in `./.venv` and nowhere else.

The standard way for [conda] as well as [python] environments is to store them
in a central folder.

**This is bad!**

Why? **Because you need to remember the name!**

So, we don't do this. Instead, we **always** create the environment for our
project in the top folder of your project in a folder called `.venv`.

This has the following advantages:

1. Activate the environment: `conda activate ./.venv`
2. Remove the environment: `rm -r .venv`
3. Create the environment: `mamba env create -p ./.venv`
4. Update the environment: `mamba env update -p ./.venv`
5. [PyCharm] and [VS Code] usually detect the folder and use that environment
   automatically.

### Rule 3: **Never** do `mamba install`

An environment **must** be reproducible.

So, instead of adding a package using `mamba install`, put it in
`environment.yml` and do `mamba env update -p ./.venv`.

## Now let's create the environment

Take another look at the `environment.yml` file we added [during the previous step](add_boilerplate.md):

```yaml
channels:
  - conda-forge
dependencies:
  - mne
  - pandas
  - ipython
```

All the packages you need go in this file. Packages can come from two sources:

1. https://anaconda.org/ : These need to be listed directly under `dependencies`
2. https://pypi.org/ : You need to add `pip` to `dependencies` and then specify 
   the packages under the `pip` category.

So, let's say, you want to add `obob_condor` which is only available on https://pypi.org/ :

```yaml
channels:
  - conda-forge
dependencies:
  - mne
  - pandas
  - pip
  - ipython
  - pip:
      - obob_condor
```

To create the environment, go to [VS Code] which has your project open, 
and select `Terminal` -> New Terminal` in the menu. Then run this command:

```
mamba env create -p ./.venv
```

When it is done, you can activate your new environment:

```
conda activate ./.venv
```

Check if it works: `which python` should return a path inside the `.venv` folder
of your project.

[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/
[gitlab_sbg]: https://git.sbg.ac.at/
[github]: https://github.com
[python]: https://www.python.org/