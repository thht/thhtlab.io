# Run some code

Congratulations! You just created your environment. Now it's time to run some
code in it!

As this is neither a python nor an MNE tutorial (if you need one look [here](../../../external_tutorials.md)),
I am just going to dump the code here.

## Let's start...

Right-click in the explorer tab and choose `New File`. Give it a name that
ends in `.py` like `hello.py`.

Put in this code:

```python
#%% imports
import mne
from pathlib import Path

#%% download the sample data
sample_data = mne.datasets.ssvep.data_path()

#%% load the data
raw = mne.io.read_raw(Path(sample_data, 'sub-01', 'ses-01', 'eeg', 'sub-01_ses-01_task-ssvep_eeg.vhdr'))
```

Notice the `#%%` lines? These mark sections that can be executed one by one.

You can now execute your code in two ways:

### Click on the "Play" button to the right.
This will open a new [python] 
terminal and execute your code there. When the file is at its end
it will also terminate the interpreter. This is great for scripts that just
do one specific job that you do not need to interact with.

### In our case, we want to see what we load, we want to plot, we want to explore.
For this purpose, there is the "Interactive Window". This is not opened
by default. But you should notice that above each cell, you see something like
`Run Cell | Run Below | Debug Cell`. You can click each of those. But for now,
please click on `Run Below`. This will open an interactive window, execute the
code and also give you a python prompt.

So, try this out: Once it has finished running, enter this in the area where
it says: `Type 'python' code here and press Shift+Enter to run`:

```python
raw.plot()
```

And hit `Shift+Enter`. This should open a nice data browser for you.

## Let's commit and sync or code

Whenever you created or modified some code, it is important to commit and
synchronize your changes to git.

Click on the `Source Control` button to the right. You are going to see
a list of all the files you created or changed there. If you hover your mouse
cursor over the `Changes` text, you will see a `+` appear. Click that.
Now your changes are staged for committing.

Now, enter a nice and informative commit message where it says `Message` and hit
`Ctrl+Enter`. Afterward hit `Sync` Changes`, wait a bit and you are good.



[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/
[gitlab_sbg]: https://git.sbg.ac.at/
[github]: https://github.com
[python]: https://www.python.org/
