# Add "Boilerplate" Files

"Boilerplate" is a term that means something like: "Essential files
that always look the same".

For our projects, there are two main boilerplate files.

## .gitignore

This file tells git, which files are not to be put under version control. Create
a file called `.gitignore` (mind the `.` at the beginning!) and put this in.

```
junit-results.xml
*.pyc
*.pyo
*.sh
*.so
*.fif
*.tar.gz
*.log
*.stc
*~
.#*
*.swp
*.lprof
*.npy
*.zip
*.fif.gz
*.nii.gz
*.tar.*
*.egg*
*.tmproj
.DS_Store
build
coverage
.cache/
.pytest_cache/
cachedir

pip-log.txt
.coverage
tags
doc/coverages
doc/samples
cover

*.orig

# PyCharm
.idea/**

# sublime
*-e

# Visual Studio Code
.vscode

# Emacs
*.py#
*.rst#

env
jobs

tmp.*

.venv
src

pip-delete-this-directory.txt

tmp_data
data

theanos_compile
```

## environment.yml

The second one defines the environment, i.e. all the packages we are going
to use. For starters, create a file called `environment.yml` and put this in:

```yaml
channels:
  - conda-forge
dependencies:
  - mne
  - pandas
  - ipython
```

## Commit the files and send them to [gitlab]

Now that we added some new files, we want to make sure to put them
under version control (i.e. git takes care if they change) and upload
those changes to [gitlab].

This is quite easy in [VS Code]. Click on the Source Control Button to the left:
![source_control_button](/_static/img/vs_code_source_control.png).

All the changes we made are now listed in the left pane. If you hover your mouse
cursor a bit to the right of the "Changes" text, you will see a `+` appear.
When you click this, our new files get "staged". Now enter a nice message in the
field above and hit the checkmark button. Our changes are now committed.

A button called "Sync Changes" will appear. Click it and everything will be
uploaded to [gitlab].


[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/
[gitlab_sbg]: https://git.sbg.ac.at/
[github]: https://github.com