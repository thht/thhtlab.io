---
lang: en-US
---


# Setup git and gitlab

## But why???

For some people, using [git] seems a bit over the top, especially as it is not
that easy to learn in the first place, and you might mess up things in the
beginning.

However, it provides some crucial advantages, if used properly like:

1. If you mess up, you can go back.
2. You can very easily synchronize your code between the bomber and your local
   PC.
3. Providing support is much easier because the person who provides support can
   check the code on their system, do changes in a new branch and then
   upload these changes and the person who asked for support can easily see
   what changed.
4. You have a backup of all your crucial code.

## What are we going to do?

We are going to set up two things. First [git], the program itself. This is
already installed on the bombers. You just need to install it on your local PC.

Second, an account on [gitlab]. This is a cloud service where you can store
your code for free.

```{note}
You can also use [github] or the [gitlab instance of the university of salzburg][gitlab_sbg].
The differences are really small...
```

## Install git
* Linux: Use your package manager
* Mac: https://git-scm.com/download/mac
* Windows: https://git-scm.com/download/win

## Get an account at [gitlab]

If you do not have an account, go to [gitlab] and set one up.

Then log in.

## Create a group for your analysis projects (optional)

Although it might not seem like this now, you are probably going to
accumulate quite some [gitlab] projects. I personally like to organize them
a bit (especially as I have other projects besides the analysis projects).
This is done using groups in [gitlab].

If you want to create one for your projects, you can do this here: https://gitlab.com/groups/new#create-group-pane

## Create ssh keys and tell [gitlab] about them

To synchronize your local code (on your local PC or your bomber) with
[gitlab], the `git` command needs to communicate with the [gitlab] server.

It also needs to authenticate you. This is done using so-called *ssh keys*.

### What are ssh keys again?

Easiest explanation possible: `ssh` is a program that lets you or your computer
talk to other computers securely. *ssh-keys* are a set of two files:

1. A public key file: You give this to [gitlab].
2. A private key file: You keep this for yourself.

The trick is that you can authenticate yourself by using (**not sending**) your
private key to another computer that only has your public key. But it does not work
the other way round.

### Ok, how do I create them?

```{admonition} Attention!
You need to do this process on your local PC as well as on the bomber!
```

First, check whether you already have keys. Open a terminal (if you are on 
Windows, look for "git bash") and type:

```
ls ~/.ssh
```

If this results in an error, or you **do not see** the following two files:

```
id_rsa  id_rsa.pub
```

you need to create keys:

```
ssh-keygen
```

Accept the default location. It is also going to ask you for a password. It is
very important to set a password if:

1. You are on a bomber
2. You are on a PC that might get used by other people

When the command has finished, you will find the two files we were looking for
previously in the `~/.ssh` folder. The one with the `.pub` extension is the
public key. This is what [gitlab] needs.

In the terminal enter:

```
cat ~/.ssh/id_rsa.pub
```

Now go to https://gitlab.com/-/profile/keys

and paste the output of the cat command in the field called `Key` and press
the button called `Add key`.

Now check, whether it has worked. Again, in the terminal enter:

```
ssh git@gitlab.com
```

You should get an output like this:

```
PTY allocation request failed on channel 0
Welcome to GitLab, @thht!
Connection to gitlab.com closed.
```

[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/
[gitlab_sbg]: https://git.sbg.ac.at/
[github]: https://github.com