# How to prepare your computer and the bomber

This tutorial is going to deal with setting up [conda],
a suitable editor like [Visual Studio Code][VS Code] or PyCharm, 
and git on both your local computer as well as the remote
machine (a.k.a. the **bomber**) so you can develop and run your data analysis.

It will also deal with keeping everything in sync if necessary using [gitlab].

Everything described in this tutorial should be like a one-time thing to do.
So you do not need to do it for every project you work on.

```{toctree}
:maxdepth: 1
conda
vscode
git
```

[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/
