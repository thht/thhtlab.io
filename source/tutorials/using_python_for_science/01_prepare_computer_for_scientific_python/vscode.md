# Install Visual Studio Code

```{admonition} Don't do this on your bomber!
You do not need to install this on your bomber. Just on your local PC!
```

To work efficiently with a programming language, you need
a good editor / IDE (integrated development environment). While there are
some good ones to choose from, we are going to use [Visual Studio Code][VS Code]
in this tutorial, because:

1. It's free
2. It runs on Linux/Mac/Windows
3. It has great remote capabilities

If you are a Linux user, just install it. Mac and Windows users
go to https://code.visualstudio.com/ and install it.

## Install helpful extensions

Visual Studio Code itself cannot do so much but it offers lots of really helpful
extensions that do the work.

Start Visual Studio Code and click on this button: ![vs_code_ext_button](/_static/img/vs_code_extensions.png)

Now, install the following extensions:

* Juypter
* Python
* Remote Development

[conda]: https://docs.conda.io/en/latest/index.html
[VS Code]: https://code.visualstudio.com/
[PyCharm]: https://www.jetbrains.com/pycharm/
[git]: https://git-scm.com/
[gitlab]: https://gitlab.com/