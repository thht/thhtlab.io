# Setting up conda

## What is [conda] and why do I need it?

[Python][python] itself is just a programming language that comes
with basic capabilities. To analyze our data, we need so-called
packages that provide the functionality that we need. These packages might
themselves depend on other packages. Just to give you a number: A current
analysis of mine specifies about 30 packages it needs but pulls in more than
300 packages because of all the dependencies.

So, it is clear that we cannot do this manually.

This is where [conda] comes in. You just give it all the packages you need
and it pulls all the dependencies.

The other great thing that [conda] does for you is **environments**.

A [conda] environment is a folder that contains its version of [python] and
all the packages your analysis needs.

So, [conda] provides two great advantages for you:

1. Your analysis can be replicated well because you specify all packages it needs.
2. The packages of one analysis do not interfere with the packages of other analyses.

## Great! Let's install it!

Unlike other programs, [conda] should be installed separately for each user.

You also need to install it once on your bomber and your local PC.

### Download and install miniconda for your system

#### On your local PC

1. Open a browser and go to https://docs.conda.io/en/latest/miniconda.html#latest-miniconda-installer-links
2. Download the correct package for your system.
3. Follow the instruction here: https://conda.io/projects/conda/en/latest/user-guide/install/index.html

#### On the bomber

1. Open a terminal
2. type: `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`
3. type: `bash Miniconda3-latest-Linux-x86_64.sh`
4. Accept the license agreement.
5. It is going to ask you this question:
   ```
   Miniconda3 will now be installed into this location:
   /home/bXXXXXXX/miniconda3

      - Press ENTER to confirm the location
      - Press CTRL-C to abort the installation
      - Or specify a different location below
   ```
6. It is **very important** that you install miniconda on the storage in your personal folder!!!!
7. So, assuming my name is "John Doe", my personal path would be `/mnt/obob/staff/jdoe`, it would enter:
   ```
   /mnt/obob/staff/jdoe/bin/miniconda
   ```
8. Wait for it to finish installing.
9. Answer yes to `Do you wish the installer to initialize Miniconda3 by running conda init?`
10. Log out and log in again
11. Open a terminal and type `which conda`. If this returns the path to your conda installation, you are good.

### Install mamba

Unfortunately, the `conda` command is super slow. Fortunately, the `mamba` command is
faster but we need to install it:

```bash
conda install -c conda-forge mamba
```


[conda]: https://docs.conda.io/en/latest/index.html
[python]: https://www.python.org/