# Fixing your python environment

Sure, python / anaconda environments are a nice thing. However, sometimes they do
break. Sometimes it is hard to figure out, why.

This tutorial assumes that you have installed anaconda/miniconda according to 
[the tutorial](../using_python_for_science/01_prepare_computer_for_scientific_python/conda.md)
and you have also created the environment [in the `.venv` folder using an `environment.yml` file as described here](../using_python_for_science/02_start_a_new_python_project/create_the_environment.md).

## Do this first

The first thing you can always try is to delete and regenerate your environment.

```bash
conda deactivate
rm -r .venv
mamba env create -p ./.venv
conda activate ./.venv
```

## I get a strange error involving `GLIBC`

This normally looks like this:

```bash
ImportError: /mnt/obob/staff/jdoe/temp/test_for_tutorial/.venv/lib/python3.10/site-packages/zmq/backend/cython/../../../../.././libstdc++.so.6: version `GLIBCXX_3.4.30' not found (required by /mnt/obob/staff/jdoe/temp/test_for_tutorial/.venv/lib/python3.10/site-packages/scipy/fft/_pocketfft/pypocketfft.cpython-310-x86_64-linux-gnu.so)
```

It basically means that some package that gets imported needs some library and does not find the correct version of it.

You can try the following things:

1. Recreate your environment
2. Add `libgcc-ng` and `libstdcxx-ng` as dependencies to your `environment.yml`.
3. Reinstall miniconda:
    1. Deactivate your environment.
    2. Find out, where miniconda was installed: `which conda` should give you the path.
    3. Let's say, it is: `/mnt/obob/staff/jdoe/bin/miniconda`, type `rm -r /mnt/obob/staff/jdoe/bin/miniconda`
    4. Follow [the steps to create install miniconda](../using_python_for_science/01_prepare_computer_for_scientific_python/conda.md)
    5. Delete and recreate your environment.
 4. Find out which package causes the error and downgrade it by one version.
    1. Look at the error message and search for the part that starts with `(required by...)`.
    2. Look for what is written after `site-packages`. This is the package causing trouble.
    3. In our case, that is `scipy`.
    4. Type `conda list` to get a list of all installed packages.
    5. Look for the offending package in the list and note the version number right next to it.
    6. Go to the `environment.yml` file and see whether the package is already required there.
       1. If yes, go to that line and exclude this version. In our case it would look like: `- scipy!=1.8.1`
       2. If no, add a line explicitly requiring that the package does not install at the offending version.
 5. Type: `mamba env update -p ./.venv`
