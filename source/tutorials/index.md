# Tutorials

This is a list of the tutorials. They do have a focus on
the environment in our group but most of it should be generally applicable.

```{admonition} Beware of **Bombers**
For anyone outside of our group: Group members get access to remote
virtual machines which we call **Bombers**. So, whenever a tutorial refers
to a bomber or doing something on the bomber, this is what I mean.
```

```{note}
These tutorial are neither python nor mne python tutorials. Other people
did a much better job at it than I could ever do. 
[Here are some suggestions](../external_tutorials.md).

```



## Using Python for Science
```{toctree}
:maxdepth: 1
:glob:

using_python_for_science/*/introduction
```

## Troubleshooting and FAQs
```{toctree}
:maxdepth: 1
:glob:

troubleshoot_and_faqs/*
```