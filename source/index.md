# Hello World

```{image} /_static/img/Thomas.jpg
:alt: Profile Picture of Thomas Hartmann
:width: 50%
:align: center
```

I am a neuroscientist working in the [Salzburg Brain Dynamics Lab](https://braindynamics.sbg.ac.at/)
at the [Paris Lodron Universität Salzburg](https://www.plus.ac.at/).

My main interests are cortical auditory processing and developing and maintaining
the necessary computing infrastructure to enable my colleagues to do their
analyses as efficiently as possible.?

If you want to find out more about my work, you can take a look
at [my profile at our group](https://braindynamics.sbg.ac.at/team/thomas-hartmann/).

This page is mainly used to publish tutorials that I think might be interesting
for people inside as well as outside our group.

## Want to leave feedback?

I would love to hear/read from you! Take a look at the right side of this your screen.
Do you see the three buttons? You can use them to leave your comments!

## What's new?

```{postlist} 5
:list-style: none
:excerpts:
:format: "{date}: {title}"
:date: "%Y-%m-%d"
```

```{toctree}
:maxdepth: 1
:hidden:

tutorials/index
external_tutorials
```